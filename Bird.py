import pygame, sys
from utils import loadImage
from random import randrange

import pygame, sys
from utils import loadImage

class Bird(pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0, speed = [2,2]):
        super(Bird, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/bird/frames/frame_00.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_01.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_02.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_03.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_04.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_05.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_06.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_07.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_08.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_09.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_10.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_11.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_12.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_13.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_14.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_15.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_16.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_17.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_18.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_19.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_20.png'))
        self.images.append(loadImage('assets/images/bird/frames/frame_21.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 30, 40)
        self.speed = speed
        self.group = pygame.sprite.Group(self)

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]