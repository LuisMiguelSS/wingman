import pygame, sys
from utils import loadImage
from random import randrange

import pygame, sys
from utils import loadImage

class Enemy(pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0):
        super(Enemy, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/enemy/frames/run_0.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_1.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_2.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_3.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_4.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_5.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_6.png'))
        self.images.append(loadImage('assets/images/enemy/frames/run_7.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 36, 35)
        self.movement_direction = 1 if randrange(2) == 1 else -1
        self.group = pygame.sprite.Group(self)

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]
    
    def set_y(self, y = 0):
        self.rect = pygame.Rect(self.rect.x, y, self.rect.width, self.rect.height)