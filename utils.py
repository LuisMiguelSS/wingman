import pygame, sys

def loadImage(name):
    image = pygame.image.load(name).convert_alpha()
    return image