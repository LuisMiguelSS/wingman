import pygame, sys
from utils import loadImage

class Player(pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0):
        super(Player, self).__init__()
        self.image = loadImage("assets/images/player/jet_small.png")
        self.rect = pygame.Rect(x, y, 52, 22)
        self.group = pygame.sprite.Group(self)