import pygame, sys
from utils import loadImage

class JetExplosion(pygame.sprite.Sprite):
    def __init__(self, x = 5, y = 5):
        super(JetExplosion, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_0.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_01.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_02.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_03.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_04.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_05.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_06.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_07.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_08.png'))
        self.images.append(loadImage('assets/images/player/explosion/frames/frame_09.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 75, 75)
        self.group = pygame.sprite.Group(self)
        self.animation_done = False

    def update(self):
        if self.index < len(self.images)-1:
            self.index += 1
            self.image = self.images[self.index]
        else:
            self.animation_done = True
            self.kill()