import pygame, sys
from utils import loadImage

class Missile(pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0, orientation = "Right"):
        super(Missile, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/missile/frames/missile_0.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_1.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_2.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_3.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_4.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_5.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_6.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_7.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_8.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_9.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_10.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_11.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_12.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_13.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_14.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_15.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_16.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_17.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_18.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_19.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_20.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_21.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_22.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_23.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_24.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_25.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_26.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_27.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_28.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_29.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_30.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_31.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_32.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_33.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_34.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_35.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_36.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_37.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_38.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_39.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_40.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_41.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_42.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_43.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_44.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_45.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_46.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_47.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_48.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_49.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_50.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_51.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_52.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_53.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_54.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_55.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_56.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_57.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_58.png'))
        self.images.append(loadImage('assets/images/missile/frames/missile_59.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 39, 13)
        self.group = pygame.sprite.Group(self)
        self.orientation = orientation

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 5
        
        if self.orientation == "Up" or self.orientation == "Down":
            self.image = pygame.transform.rotate(self.images[self.index], -90)
        elif self.orientation == "Left":
            self.image = pygame.transform.rotate(self.images[self.index], 180)
        else:
            self.image = self.images[self.index]