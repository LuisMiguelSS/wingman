import pygame, sys
from utils import loadImage
from random import randrange

import pygame, sys
from utils import loadImage

class Cloud(pygame.sprite.Sprite):
    def __init__(self, x = 0, y = 0):
        super(Cloud, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/cloud/frame_00.png'))
        self.images.append(loadImage('assets/images/cloud/frame_01.png'))
        self.images.append(loadImage('assets/images/cloud/frame_02.png'))
        self.images.append(loadImage('assets/images/cloud/frame_03.png'))
        self.images.append(loadImage('assets/images/cloud/frame_04.png'))
        self.images.append(loadImage('assets/images/cloud/frame_05.png'))
        self.images.append(loadImage('assets/images/cloud/frame_06.png'))
        self.images.append(loadImage('assets/images/cloud/frame_07.png'))
        self.images.append(loadImage('assets/images/cloud/frame_08.png'))
        self.images.append(loadImage('assets/images/cloud/frame_09.png'))
        self.images.append(loadImage('assets/images/cloud/frame_10.png'))
        self.images.append(loadImage('assets/images/cloud/frame_11.png'))
        self.images.append(loadImage('assets/images/cloud/frame_12.png'))
        self.images.append(loadImage('assets/images/cloud/frame_13.png'))
        self.images.append(loadImage('assets/images/cloud/frame_14.png'))
        self.images.append(loadImage('assets/images/cloud/frame_15.png'))
        self.images.append(loadImage('assets/images/cloud/frame_16.png'))
        self.images.append(loadImage('assets/images/cloud/frame_17.png'))
        self.images.append(loadImage('assets/images/cloud/frame_18.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 188, 188)
        self.movement_direction = 1 if randrange(2) == 1 else -1
        self.group = pygame.sprite.Group(self)

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]
    
    def set_y(self, y = 0):
        self.rect = pygame.Rect(self.rect.x, y, self.rect.width, self.rect.height)