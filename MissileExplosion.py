import pygame, sys
from utils import loadImage

class MissileExplosion(pygame.sprite.Sprite):
    def __init__(self, x = 5, y = 5):
        super(MissileExplosion, self).__init__()
        self.images = []
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_00.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_01.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_02.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_03.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_04.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_05.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_06.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_07.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_08.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_09.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_10.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_11.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_12.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_13.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_14.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_15.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_16.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_17.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_18.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_19.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_20.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_21.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_22.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_23.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_24.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_25.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_26.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_27.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_28.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_29.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_30.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_31.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_32.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_33.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_34.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_35.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_36.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_37.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_38.png'))
        self.images.append(loadImage('assets/images/missile/explosion/frames/frame_39.png'))

        self.index = 0
        self.image = self.images[self.index]
        self.rect = pygame.Rect(x, y, 63, 47)
        self.group = pygame.sprite.Group(self)
        self.animation_done = False

    def update(self):
        if self.index < len(self.images)-1:
            self.index += 1
            self.image = self.images[self.index]
        else:
            self.animation_done = True
            self.kill()