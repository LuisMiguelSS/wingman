'''Game Imports'''
import sys
import time
from random import randrange

import pygame
from pygame.locals import *

from utils import *
from Missile import *
from MissileExplosion import *
from Player import *
from JetExplosion import *
from Enemy import *
from Cloud import *
from Bird import *

#***********************************
# Wingman info
#***********************************
# Software developed by
# Luis Miguel Soto Sánchez (@LuisMiguelSS)
# on github.
#
# Copyright - All rigths reserved 2020

#***********************************
# Assets credit (not all may appear):
#***********************************
# Sky Background (assets/images/sky.png):
#   opengameart.org/users/bart
# Enemy Frames (assets/images/enemy/frames/run_x.png):
#   opengameart.org/users/pzuh
# Bird Frames (assets/images/BIRD/frames/frame_x.png):
#   www.pinterest.es/pin/436497388859683801/
# -Music (BlazerRail.ogg):
#   www.dl-sounds.com
# -SFX Button (ButtonClick.ogg):
#   freesound.org/people/Christopherderp
# -SFX Explosion (Explosion.ogg):
#   freesound.org/people/rendensh
# -SFX Jet (JetLoop.ogg):
#   www.soundsnap.com/tags/jet_engine
# -SFX Missile Explosion (MissileExplosion.ogg):
#   www.audioblocks.com/stock-audio/explosion-firework-boom-single-rl-imphldrk0wxx33k.html

pygame.init()

# Variables

#
# Game
#
SCR_TITLE = "Wingman!"
SCR_WIDTH = 800
SCR_HEIGHT = 420

SCR_MIN_WIDTH = 500
SCR_MIN_HEIGHT = 300

# Colors
COLOR_SKY = (138, 229, 255)
COLOR_GROUND = (64, 51, 43)
COLOR_WHITE = (255, 255, 255)
COLOR_BLACK = (0, 0, 0)
COLOR_GREY = (211, 211, 211)
COLOR_GREEN = (100, 221, 23)
COLOR_RED = (244, 67, 54)

# Sky and ground rectangles
SKY_AREA = pygame.Rect(0, 0, SCR_WIDTH, SCR_HEIGHT // 5*4)
SKY_BG = pygame.transform.scale(pygame.image.load("assets/images/sky.png"), \
    (SKY_AREA.width, SKY_AREA.height))
GROUND_AREA = pygame.Rect(0, SCR_HEIGHT// 5*4, SCR_WIDTH, SCR_HEIGHT// 5*4)
GROUND_BG = pygame.transform.scale(pygame.image.load("assets/images/ground.png"), \
    (GROUND_AREA.width, GROUND_AREA.height))

# prepare screen
SCREEN = pygame.display.set_mode((SCR_WIDTH, SCR_HEIGHT), \
    pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)
pygame.display.set_caption(SCR_TITLE)

# Fonts & text
FONT = ""
try:
    FONT = pygame.font.Font("assets/font/AmaticSC-Regular.ttf", 60)
except FileNotFoundError:
    print("[Log] Custom font not found, loading system's one.")
    FONT = pygame.font.SysFont('arial', 60)

SCR_TITLE = FONT.render(SCR_TITLE, True, COLOR_BLACK, None)
SCR_TITLE_RECTANGLE = SCR_TITLE.get_rect()
SCR_TITLE_RECTANGLE.center = (SCR_WIDTH // 2, SCR_HEIGHT // 4)

HELP_FONT = pygame.font.SysFont('arial', 17)
HELP_TEXT = HELP_FONT.render(\
    "Usa las flechas para moverte y pulsa la barra espaciadora para disparar.", True, \
    COLOR_BLACK, None)

# Load Music
BG_MUSIC = pygame.mixer.Sound("assets/sounds/BlazerRail.ogg")
SFX_BUTTON = pygame.mixer.Sound("assets/sounds/ButtonClick.ogg")
SFX_EXPLOSION = pygame.mixer.Sound("assets/sounds/Explosion.ogg")
SFX_JET = pygame.mixer.Sound("assets/sounds/JetLoop.ogg")
SFX_MISSILE_EXPLOSION = pygame.mixer.Sound("assets/sounds/MissileExplosion.ogg")

pygame.mixer.Channel(0).play(BG_MUSIC, -1, 0)
pygame.mixer.Channel(0).set_volume(0.2)

# Play Button

# play button area
BTN_WIDTH = 100
BTN_HEIGHT = 50
BTN_X = int(SCR_WIDTH/2 - BTN_WIDTH/2)
BTN_Y = int(SCR_HEIGHT/2 - BTN_HEIGHT/2 + 15)
PLAY_BTN = pygame.Rect(BTN_X, BTN_Y, BTN_WIDTH, BTN_HEIGHT)

# play button font
PLAY_BTN_FONT = pygame.font.SysFont('arial', 25)
PLAY_BTN_TITLE = PLAY_BTN_FONT.render("Jugar", True, COLOR_BLACK, None)
PLAY_BTN_TITLE_RECTANGLE = PLAY_BTN_TITLE.get_rect()
PLAY_BTN_TITLE_RECTANGLE.center = (BTN_X + int(BTN_WIDTH/2), BTN_Y + int(BTN_HEIGHT/2))


# Game control
MAX_SCORE = 0
CURRENT_SCORE = 0
IS_PLAYING = False
CLOCK = pygame.time.Clock()
ENEMIES = []
MISSILES = []
CLOUDS = []
MISSILE_EXPLOSIONS = []
BIRD = Bird(randrange(0, SCR_WIDTH), randrange(0, SKY_AREA.height))

PLAYER = Player(SCR_WIDTH/2)
PLAYER_CURRENT_ORIENTATION = "Right"
PLAYER_HORIZONTAL_ORIENTATION = "Right"
PLAYER_ANIMATED_HEIGHT = 0

WIN = False
LOSE = False
PLAYER_EXPLOSION = None
SHOULD_ANIMATE_EXPLOSION = False

PREVIOUS_SHOOT_TIME = pygame.time.get_ticks()

#
# Functions
#
def update_sizes(screen_width, screen_height):
    global SCR_WIDTH
    global SCR_HEIGHT
    global SKY_AREA
    global SKY_BG
    global GROUND_AREA
    global GROUND_BG
    global SCREEN
    global SCR_TITLE_RECTANGLE
    global BTN_X
    global BTN_Y
    global PLAY_BTN
    global PLAY_BTN_TITLE_RECTANGLE
    global PLAYER
    global BIRD

    SCR_WIDTH = screen_width
    SCR_HEIGHT = screen_height

    SKY_AREA = pygame.Rect(0, 0, SCR_WIDTH, int(SCR_HEIGHT/ 5*4))
    SKY_BG = pygame.transform.scale(pygame.image.load("assets/images/sky.png"), \
        (SKY_AREA.width, SKY_AREA.height))
    GROUND_AREA = pygame.Rect(0, int(SCR_HEIGHT/ 5*4), SCR_WIDTH, int(SCR_HEIGHT / 5))
    GROUND_BG = pygame.transform.scale(pygame.image.load("assets/images/ground.png"), \
        (GROUND_AREA.width, GROUND_AREA.height))

    SCREEN = pygame.display.set_mode((SCR_WIDTH, SCR_HEIGHT), \
        pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)
    SCR_TITLE_RECTANGLE = SCR_TITLE.get_rect()
    SCR_TITLE_RECTANGLE.center = (SCR_WIDTH // 2, SCR_HEIGHT // 4)

    BTN_X = int(SCR_WIDTH/2 - BTN_WIDTH/2)
    BTN_Y = int(SCR_HEIGHT/2 - BTN_HEIGHT/2 + 15)
    PLAY_BTN = pygame.Rect(BTN_X, BTN_Y, BTN_WIDTH, BTN_HEIGHT)
    PLAY_BTN_TITLE_RECTANGLE = PLAY_BTN_TITLE.get_rect()
    PLAY_BTN_TITLE_RECTANGLE.center = (BTN_X + int(BTN_WIDTH/2), BTN_Y + int(BTN_HEIGHT/2))
    PLAYER = Player(SCR_WIDTH/2)

    BIRD = Bird(randrange(0, SCR_WIDTH), randrange(0, SKY_AREA.height - BIRD.rect.height))

    # Update ENEMIES
    for enemy in ENEMIES:
        enemy.set_y(GROUND_AREA[1] - 30)

    # Update CLOUDS
    for cloud in CLOUDS:
        cloud.set_y(GROUND_AREA[1] - 188)

    paint_bg()

def paint_bg():
    SCREEN.fill(COLOR_SKY, SKY_AREA) # To avoid an empty black space if the sky image does not load
    SCREEN.fill(COLOR_GROUND, GROUND_AREA) # Avoid empty black space if image does not load
    SCREEN.blit(SKY_BG, SKY_AREA)
    SCREEN.blit(GROUND_BG, GROUND_AREA)

def paint_score():
    global CURRENT_SCORE
    score_font = pygame.font.SysFont('arial', 17)
    score_text = score_font.render("Puntos: " + str(CURRENT_SCORE), True, COLOR_BLACK, None)
    SCREEN.blit(score_text, (0, 0))

# Paints the home button 'Play' with or without
# the hovered effect, as marked by the received boolean.
def paint_button_hovered(hovered):
    if hovered:
        pygame.draw.rect(SCREEN, COLOR_WHITE, PLAY_BTN)
    else:
        pygame.draw.rect(SCREEN, COLOR_GREY, PLAY_BTN)
    SCREEN.blit(PLAY_BTN_TITLE, PLAY_BTN_TITLE_RECTANGLE)

# Paints the given imgage in the given orientation,
# supporting horizontal, vertical and diagonal movement.
def orientate_image(image, rect, orientation):
    if orientation == "Right":
        SCREEN.blit(pygame.transform.flip(image, True, False), rect)
    elif orientation == "Left":
        SCREEN.blit(image, rect)
    elif orientation == "Down":
        if PLAYER_HORIZONTAL_ORIENTATION == "Left":
            SCREEN.blit(pygame.transform.rotate(image, 45), rect)
        elif PLAYER_HORIZONTAL_ORIENTATION == "Right":
            SCREEN.blit(pygame.transform.flip(pygame.transform.rotate(image, 45), True, False), \
                rect)
        else:
            SCREEN.blit(pygame.transform.flip(pygame.transform.rotate(image, 90), True, False), \
                rect)
    elif orientation == "Up":
        if PLAYER_HORIZONTAL_ORIENTATION == "Left":
            SCREEN.blit(pygame.transform.rotate(image, -45), rect)
        elif PLAYER_HORIZONTAL_ORIENTATION == "Right":
            SCREEN.blit(pygame.transform.flip(pygame.transform.rotate(image, -45), True, False), \
                rect)
        else:
            SCREEN.blit(pygame.transform.flip(pygame.transform.rotate(image, -90), True, False), \
                rect)

#
# Player
def explode_plane():
    global PLAYER_EXPLOSION

    # Sound - Explosion
    pygame.mixer.Channel(2).play(SFX_EXPLOSION, 0, 0)
    pygame.mixer.Channel(2).set_volume(5)

    # Sprite - Explosion
    PLAYER_EXPLOSION = JetExplosion(PLAYER.rect.left - PLAYER.rect.width/2, \
        PLAYER.rect.top - PLAYER_ANIMATED_HEIGHT/1.5)
    paint_bg()
    PLAYER_EXPLOSION.group.draw(SCREEN)
#
# MISSILES
def paint_missiles():
    for missile in MISSILES:
        missile.update()
        missile.group.draw(SCREEN)

def paint_missile_explosions():
    for explosion in MISSILE_EXPLOSIONS:
        if not explosion.animation_done:
            explosion.update()
            explosion.group.draw(SCREEN)
        else:
            MISSILE_EXPLOSIONS.remove(explosion)

def move_missiles():
    for missile in MISSILES:
        # Check collision
        if missile.rect.bottom < GROUND_AREA[1]:
            missile.rect.move_ip(0, 2)
        else:
            missile.remove()
            MISSILES.remove(missile)

            # Add explosion sprite
            new_missile_explosion = MissileExplosion(missile.rect.x - missile.rect.width/2, \
                missile.rect.y - missile.rect.height*1.5)
            MISSILE_EXPLOSIONS.append(new_missile_explosion)

            # Add explosion sound
            pygame.mixer.Channel(4).play(SFX_MISSILE_EXPLOSION, 0, 0)
            pygame.mixer.Channel(4).set_volume(2)

#
# ENEMIES
def init_enemies(number=6):
    for _ in range(1, number + 1):
        ENEMIES.append(Enemy(randrange(SCR_WIDTH-36), GROUND_AREA[1] - 30))

def paint_enemies():
    global WIN
    global CURRENT_SCORE
    killed_enemies = []

    if len(ENEMIES) == 0:
        WIN = True

    for enemy in ENEMIES:
        # Check if it's affected by the explosion
        for missile in MISSILE_EXPLOSIONS:
            if missile.rect.contains(enemy.rect):
                killed_enemies.append(enemy)

        enemy.update()
        enemy.group.draw(SCREEN)

    for enemy in killed_enemies:
        if ENEMIES.__contains__(enemy):
            ENEMIES.remove(enemy)
            CURRENT_SCORE += 1

def move_enemies():
    for enemy in ENEMIES:
        enemy.rect.move_ip(enemy.movement_direction * 2, 0)
        if not SCREEN.get_rect().contains(enemy.rect):
            enemy.movement_direction *= -1

#
# Clouds
def init_clouds(number=3):
    CLOUDS.clear()
    for _ in range(1, number + 1):
        CLOUDS.append(Cloud(randrange(SCR_WIDTH - 188), randrange(GROUND_AREA[1] - 188)))

def paint_clouds():
    for cloud in CLOUDS:
        cloud.update()
        cloud.group.draw(SCREEN)

def move_clouds():
    for cloud in CLOUDS:
        cloud.rect.move_ip(cloud.movement_direction, 0)
        if not SCREEN.get_rect().contains(cloud.rect):
            cloud.movement_direction *= -1

#
# Bird
def paint_bird():
    BIRD.update()
    BIRD.group.draw(SCREEN)

def move_bird():

    if (BIRD.rect.x < 0) or (BIRD.rect.x > SCR_WIDTH - BIRD.rect.width):
        BIRD.speed[0] *= -1
    if (BIRD.rect.y < 0) or (BIRD.rect.y > SKY_AREA.height - BIRD.rect.height):
        BIRD.speed[1] *= -1

    BIRD.rect.x = BIRD.rect.x + BIRD.speed[0]
    BIRD.rect.y = BIRD.rect.y + BIRD.speed[1]

#
# Show Menu
def show_menu():
    global IS_PLAYING
    global SHOULD_ANIMATE_EXPLOSION
    IS_PLAYING = False

    paint_bg()

    if SHOULD_ANIMATE_EXPLOSION:
        SHOULD_ANIMATE_EXPLOSION = False
        orientate_image(PLAYER.image, PLAYER.rect, "")
        explode_plane()
        pygame.mixer.Channel(3).stop()

    # Show title and help
    SCREEN.blit(SCR_TITLE, SCR_TITLE_RECTANGLE)
    SCREEN.blit(HELP_TEXT, (0, 0))


#
# WIN
def game_win():
    global WIN
    global LOSE
    WIN = True
    LOSE = False

    SCREEN.fill(COLOR_GREEN, SCREEN.get_rect())
    score_font = pygame.font.SysFont('arial', 37)
    score_text = score_font.render("¡HAS GANADO!", True, COLOR_BLACK, None)
    SCREEN.blit(score_text, (0, int(SCR_HEIGHT/2)))
    pygame.display.flip()
    pygame.time.wait(2000)

    reset()

#
# LOSE
def game_lose():
    global WIN
    global LOSE
    WIN = False
    LOSE = True

    SCREEN.fill(COLOR_RED, SCREEN.get_rect())
    score_font = pygame.font.SysFont('arial', 37)
    score_text = score_font.render("Has perdido...", True, COLOR_BLACK, None)
    SCREEN.blit(score_text, (0, int(SCR_HEIGHT/2)))
    pygame.display.flip()
    pygame.time.wait(2000)

    show_menu()
    reset()

def reset():
    global WIN
    global LOSE
    global CURRENT_SCORE
    global MAX_SCORE
    global ENEMIES
    global MISSILES
    global MISSILE_EXPLOSIONS
    global PLAYER

    WIN = False
    LOSE = False
    CURRENT_SCORE = 0
    MAX_SCORE = 0
    ENEMIES = []
    MISSILES = []
    MISSILE_EXPLOSIONS = []

    show_menu()

#
# Exit
def exit_game():
    pygame.mixer.quit()
    pygame.quit()
    sys.exit()

#******************
# Game MAIN
#**************

while True:

    # Refresh background
    paint_bg()

    # Check Events
    for event in pygame.event.get():
        # Quit Event
        if event.type == pygame.QUIT:
            exit_game()
        # Resize Event
        elif event.type == pygame.VIDEORESIZE:
            newWidth, newHeight = event.size

            if newWidth < SCR_MIN_WIDTH:
                newWidth = SCR_MIN_WIDTH
            if newHeight < SCR_MIN_HEIGHT:
                newHeight = SCR_MIN_HEIGHT

            update_sizes(newWidth, newHeight)
        pygame.event.pump()

    # Keyboard Input
    if IS_PLAYING:
        KEYS = pygame.key.get_pressed()

        # Arrow ⇨
        if KEYS[pygame.K_RIGHT] and PLAYER.rect.left + PLAYER.rect.width < SCR_WIDTH - 3:
            PLAYER.rect.left += 3
            PLAYER_CURRENT_ORIENTATION = "Right"
            PLAYER_HORIZONTAL_ORIENTATION = "Right"
            PLAYER_ANIMATED_HEIGHT = PLAYER.rect.height

        # Arrow ⇦
        if KEYS[pygame.K_LEFT] and PLAYER.rect.left > 0 + 3:
            PLAYER.rect.left -= 3
            PLAYER_CURRENT_ORIENTATION = "Left"
            PLAYER_HORIZONTAL_ORIENTATION = "Left"
            PLAYER_ANIMATED_HEIGHT = PLAYER.rect.height

        # Arrow ⇩
        if KEYS[pygame.K_DOWN]:
            if not KEYS[pygame.K_RIGHT] and not KEYS[pygame.K_LEFT]:
                PLAYER_HORIZONTAL_ORIENTATION = ""
                PLAYER.rect.top += 4
                PLAYER_ANIMATED_HEIGHT = PLAYER.rect.height
            else:
                PLAYER.rect.top += 3
                PLAYER_ANIMATED_HEIGHT = PLAYER.rect.width
            PLAYER_CURRENT_ORIENTATION = "Down"

        # Arrow ⇧
        if KEYS[pygame.K_UP]:
            if PLAYER.rect.top >= 0:
                if not KEYS[pygame.K_RIGHT] and not KEYS[pygame.K_LEFT]:
                    PLAYER_HORIZONTAL_ORIENTATION = ""
                    PLAYER.rect.top -= 3
                    PLAYER_ANIMATED_HEIGHT = PLAYER.rect.height
                else:
                    PLAYER.rect.top -= 2
                    PLAYER_ANIMATED_HEIGHT = PLAYER.rect.width
                PLAYER_CURRENT_ORIENTATION = "Up"

        # Spacebar
        if KEYS[pygame.K_SPACE]:
            CURRENT_TIME = pygame.time.get_ticks()
            if CURRENT_TIME - PREVIOUS_SHOOT_TIME > 500:
                PREVIOUS_SHOOT_TIME = CURRENT_TIME
                MISSILES.append(Missile(PLAYER.rect.left + PLAYER.rect.width/2, \
                    PLAYER.rect.bottom, PLAYER_CURRENT_ORIENTATION))

        IS_KEY_PRESSED = False
        for key in pygame.key.get_pressed():
            if key == 1:
                IS_KEY_PRESSED = True

        # Make Player fall
        if not IS_KEY_PRESSED:
            if SKY_AREA.colliderect(PLAYER.rect):
                PLAYER.rect.top += 1

            if PLAYER.rect.left + PLAYER.rect.width < SCR_WIDTH - 3 and PLAYER.rect.left > 0 + 3:
                if PLAYER_HORIZONTAL_ORIENTATION == "Left":
                    PLAYER.rect.left -= 1
                elif PLAYER_HORIZONTAL_ORIENTATION == "Right":
                    PLAYER.rect.left += 1

            pygame.mixer.Channel(3).set_volume(0)
        else:
            pygame.mixer.Channel(3).set_volume(0.4)

    #
    # Collisions
    # (If hit ground or BIRD)
    if GROUND_AREA.colliderect(PLAYER.rect) or \
        BIRD.rect.colliderect(PLAYER.rect): # Ground
        SHOULD_ANIMATE_EXPLOSION = True
        print("Lose set to true")

        LOSE = True
        PLAYER = Player(SCR_WIDTH/2)

    if IS_PLAYING:
        paint_score()
        orientate_image(PLAYER.image, PLAYER.rect, PLAYER_CURRENT_ORIENTATION)

        # Refresh BIRD
        paint_bird()
        move_bird()

        # Refresh CLOUDS
        paint_clouds()
        move_clouds()

        # Refresh ENEMIES
        if len(ENEMIES) > 0 and MAX_SCORE > 0:
            paint_enemies()
            move_enemies()
            if LOSE:
                game_lose()
        else:
            game_win()

        # Refresh MISSILES
        paint_missiles()
        move_missiles()

        # Refresh missile explosions
        paint_missile_explosions()


    # Player is not playing
    else:

        # Show Menu
        show_menu()

        # Mouse Position
        MOUSE = pygame.mouse.get_pos()

        if BTN_X+BTN_WIDTH > MOUSE[0] > BTN_X and BTN_Y+BTN_HEIGHT > MOUSE[1] > BTN_Y:
            if pygame.mouse.get_pressed()[0] == 1:

                PLAYER = Player(SCR_WIDTH/2)

                # ENEMIES
                init_enemies()
                paint_enemies()

                # Clouds
                init_clouds(4)
                paint_clouds()

                # Others
                MAX_SCORE = len(ENEMIES)
                CURRENT_SCORE = 0
                IS_PLAYING = True

                orientate_image(PLAYER.image, PLAYER.rect, PLAYER_CURRENT_ORIENTATION)

                # Sound
                pygame.mixer.Channel(1).play(SFX_BUTTON, 1, 0)
                pygame.mixer.Channel(1).set_volume(5)

                pygame.mixer.Channel(3).play(SFX_JET, -1, 0)
                pygame.mixer.Channel(3).set_volume(0.4)
            else:
                paint_button_hovered(True)
        else:
            paint_button_hovered(False)

    # Clear screen and frames
    pygame.display.update()
    CLOCK.tick(60)

# CLOSE
exit_game()
